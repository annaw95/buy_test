from selenium import webdriver

from webdriver_manager.chrome import ChromeDriverManager

options = webdriver.ChromeOptions()
options.add_argument('--start-maximized')

driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=options)

driver.get("http://automationpractice.com/index.php")
driver.find_element_by_class_name('sf-with-ul')
driver.find_element_by_id("header_logo")
driver.find_element_by_class_name("shopping_cart")
driver.find_element_by_id("newsletter-input")
driver.find_element_by_class_name("homefeatured")
driver.find_element_by_css_selector("#header_logo")

driver.find_elements_by_css_selector(".twitter a")

driver.find_element_by_xpath("//div[@id='header_logo']")
driver.find_elements_by_xpath("//*@id='contact-link']")


driver.quit()

