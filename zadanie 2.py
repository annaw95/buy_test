from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


search_selector = (By.CLASS_NAME, "login")
email_field_selector = (By.ID, "email")
pass_field_selector = (By.ID, "passwd")
submit_selector = (By.ID, "SubmitLogin")
addresses_selector = (By.XPATH, "//*[@title='Addresses']")
credit_slips_selector = (By.XPATH, "//*[@title='Credit slips']")
address_row_selector = (By.CSS_SELECTOR, ".bloc_adresses.row")
credit_slips_alert = (By.CSS_SELECTOR, ".alert.alert-warning")

options = webdriver.ChromeOptions()
options.add_argument('--start-maximized')

driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=options)

driver.get("http://automationpractice.com/index.php")


driver.find_element(*search_selector).click()

WebDriverWait(driver, 3).until(EC.presence_of_element_located(email_field_selector)).send_keys("seleniumwsh@gmail.com")
driver.find_element(*pass_field_selector).send_keys("test123")
driver.find_element(*submit_selector).click()
driver.find_element(*addresses_selector).click()

WebDriverWait(driver, 3).until(EC.presence_of_element_located(address_row_selector))

driver.back()

driver.find_element(*credit_slips_selector).click()
WebDriverWait(driver, 3).until(EC.invisibility_of_element_located(credit_slips_alert))
