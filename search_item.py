from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


search_selector = (By.ID, "search_query_top")
submit_search_selector = (By.NAME, "submit_search")
alert_box_selector = (By.CSS_SELECTOR, ".alert.alert-warning")

options = webdriver.ChromeOptions()
options.add_argument('--start-maximized')

driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=options)

driver.get("http://automationpractice.com/index.php")


# driver.find_element_by_id(search_selector).send_keys("test")
# driver.find_element_by_name(submit_search_selector).click()
# driver.find_element_by_css_selector(alert_box_selector)

driver.find_element(*search_selector).send_keys("test")
driver.find_element(*submit_search_selector).click()
#driver.find_element(*alert_box_selector)

WebDriverWait(driver, 3).until(EC.presence_of_element_located(alert_box_selector))

driver.quit()
