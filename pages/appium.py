import os
from appium.webdriver import Remote


desired_caps = {}
desired_caps['platformName'] = 'Android'
desired_caps['platformVersion'] = '10'
desired_caps['deviceName'] = 'Android Emulator'

driver = Remote('http://localhost:4723/wd/hub', desired_capabilities=desired_caps)
