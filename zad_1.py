import math

def calculate_fuel(mass):
    fuel = mass // 3 - 2
    return fuel

print(calculate_fuel(10))
mass_1 = calculate_fuel(12)
mass_2 = calculate_fuel(14)
mass_3 = calculate_fuel(1969)
mass_4 = calculate_fuel(100756)

the_fuel_sum = mass_1 + mass_2 + mass_3 + mass_4
print(the_fuel_sum)
print(mass_4)

def test_1():
    assert calculate_fuel(12) ==2

def test_negative():
